//
//  DRView.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/1/20.
//

import SwiftUI
import PencilKit
import CoreGraphics
import CoreImage


struct DRView: View {
    @StateObject var model = DRInferenceModel()
    @State private var canvasView = PKCanvasView()
    
    var body: some View {
        
        VStack {
            Text(String(model.inferenceResult.maxValue))
                .font(.title)
                .padding()
            
            HStack {
                SoftMaxOutputCard(model: model)
                    .frame(width: 150, height: 150)
                    .padding()
                
                DrawnImageView(model: model)
                    .padding()
            }
            
            Button(
                action: {
                    print(">>> Guessing...")
                    model.inferDigit(
                        from: self.canvasView.drawing.image(from: self.canvasView.drawing.bounds, scale: 1.0)
                    )
                    canvasView.drawing = PKDrawing()
                },
                label: {
                    HStack {
                        Text("Guess")
                            .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                            .fontWeight(.medium)
                    }
                    .frame(width: 350, height: 50)
                    .foregroundColor(.white)
                    .background(Color("Ink"))
                    .cornerRadius(15)
                }
            )

            PKCanvasWrapper(canvasView: $canvasView)
                .frame(width: 350, height: 350)
                .cornerRadius(15)
                .padding()
            
            Spacer()
        }

    }
}

struct DrawnDigitView_Previews: PreviewProvider {
    static var previews: some View {
        DRView()
    }
}



//
//  DrawnImageView.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/3/20.
//

import SwiftUI

struct DrawnImageView: View {
    @ObservedObject var model: DRInferenceModel
    
    var body: some View {
        Image(model.inferenceResult.image, scale: 1.0, label: Text(""))
            .resizable()
            .frame(width: 150, height: 150)
            .cornerRadius(15)
    }
}


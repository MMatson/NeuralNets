//
//  DRNetworkContainer.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/6/20.
//

import Foundation
import CoreGraphics
import CoreImage


class DRNetworkContainer {
    
    private var inferenceImage: CGImage?
    
    private lazy var networkCore: TorchModule = {
        if let  modelPath = Bundle.main.path(forResource: "scnn", ofType: "pt"),
            let module = TorchModule(fileAtPath:  modelPath) {
            return module
        } else {
            fatalError("Cannot find the neural network model...")
        }
    }()
    
    
    //MARK: Network Functions
    
    func performSoftMaxInference(on image: CGImage) -> [Float]? {
        var imageArray: [UInt8]
        if inferenceImage != nil {
            imageArray = pixelUInt8Array(from: inferenceImage!)
        } else {
            print(">>> No inference image found...")
            return nil
        }
        var imageBuffer = normalizedFloatData(from: imageArray)
        
        guard let output = networkCore.inferSoftmaxDistribution(image: UnsafeMutableRawPointer(&imageBuffer)) else {
            return nil
        }
        
        print("Output: ", output)
        var typedOutput = [Float]()
        for element in output {
            let typedElement = element as! Float
            typedOutput.append(typedElement)
        }
        
        let e = Darwin.M_E
        let distribution = typedOutput.map({pow(Float(e), $0)})
        
        print("Distribution: ", distribution)
        
        print("Max", distribution.firstIndex(of: distribution.max() ?? 0.0) ?? -1)
        
        return distribution
    }
    
    func processMNISTImageForNetwork(from drawnImage: CGImage) -> CGImage? {
        print(" - Resizing image...")
        let mnistSize = CGSize(width: 28, height: 28)
        
        if let resizedImage = resize(image: drawnImage, newSize: mnistSize, padding: 4.0) {
            print(" - Converting image to grayscale...")
            if let grayscaleImage = grayscale(image: resizedImage) {
                print(" - Inverting image...")
                inferenceImage = grayscaleImage
                if let invertedImage = invert(image: grayscaleImage) {
                    return invertedImage
                }
            }
        }
        return nil
    }

    func createEmptyMNISTImage() -> CGImage {
        let context = CGContext(
            data: nil,
            width: 28,
            height: 28,
            bitsPerComponent: 8,
            bytesPerRow: 28,
            space: CGColorSpaceCreateDeviceGray(),
            bitmapInfo: CGImageAlphaInfo.none.rawValue
        )
        return context!.makeImage()!
    }
    
    
    //MARK: Image Manipluation
    //TODO: Move to ImageProcessing
    
    private func resize(image: CGImage, newSize: CGSize, padding: CGFloat = 0) -> CGImage? {
        
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        let imageSize = CGSize(width: image.width, height: image.height)
        let widthRatio = (newSize.width - (padding * 2)) / imageSize.width
        let heightRatio = (newSize.height - (padding * 2)) / imageSize.height

        print("width: \(image.width), height: \(image.height), widthRatio: \(widthRatio), heightRatio: \(heightRatio)")
        
        var scale: CGFloat
        widthRatio < heightRatio ? (scale = widthRatio) : (scale = heightRatio)
        
        let imageRect = CGRect(
            x: (newSize.width - (imageSize.width * scale)) / 2,
            y: (newSize.height - (imageSize.height * scale)) / 2,
            width: imageSize.width * scale,
            height: imageSize.height * scale
        )
        print("scale: \(scale)")
        guard let context = CGContext(
                data: nil,
                width: Int(newSize.width),
                height: Int(newSize.height),
                bitsPerComponent: image.bitsPerComponent,
                bytesPerRow: image.bytesPerRow,
                space: image.colorSpace!,
                bitmapInfo: image.bitmapInfo.rawValue
        )
        else {
            return nil
        }
        print("mnistRect: \(newRect)")
        print("imageRect: \(imageRect)")
        context.setFillColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        context.fill(newRect)
        //context.draw(createEmptyMNISTImage(), in: mnistRect)
        //context.interpolationQuality = .high
        context.draw(image, in: imageRect)
        return context.makeImage()!
    }

    private func grayscale(image: CGImage) -> CGImage? {
        guard let context = CGContext(
            data: nil,
            width: image.width,
            height: image.height,
            bitsPerComponent: 8,
            bytesPerRow: 28,
            space: CGColorSpaceCreateDeviceGray(),
            bitmapInfo: CGImageAlphaInfo.none.rawValue
        ) else {
            return nil
        }
        let imageRect = CGRect(origin: CGPoint.zero, size: CGSize(width: image.width, height: image.height))
        context.setFillColor(gray: 1.0, alpha: 1.0)
        context.fill(imageRect)
        context.draw(image, in: imageRect)
        return context.makeImage()
    }

    private func invert(image: CGImage) -> CGImage? {
        let inputImage = CIImage(cgImage: image)
        let inversionFilter = CIFilter(name: "CIColorInvert")!
        inversionFilter.setValue(inputImage, forKey: kCIInputImageKey)
        return CIContext(options: nil).createCGImage(inversionFilter.outputImage!, from: inputImage.extent)
    }
    
    
    //MARK: Data Manipulation
    //TODO: Move to DataProcessing
    
    private func pixelUInt8Array(from image: CGImage) -> [UInt8] {
        let width = image.width, height = image.height
        let bytesPerRow = image.bytesPerRow
        let totalBytes = height * bytesPerRow
        let bitsPerComponent = image.bitsPerComponent
        
        var intArray = [UInt8](repeating: 0, count: totalBytes)
        guard let context = CGContext(
            data: &intArray,
            width: width,
            height: height,
            bitsPerComponent: bitsPerComponent,
            bytesPerRow: bytesPerRow,
            space: CGColorSpaceCreateDeviceGray(),
            bitmapInfo: 0
        ) else {
            fatalError("Cannot create CGContext...")
        }
        context.draw(image, in: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: height)))
        return intArray
    }
    
    private func normalizedFloatData(from intArray: [UInt8]) -> [Float] {
        var floatArray = [Float]()
        intArray.forEach{ floatArray.append(Float($0) / 255.0) }
        floatArray = floatArray.map{ Float(1 - $0)}
        return floatArray
    }
    
}






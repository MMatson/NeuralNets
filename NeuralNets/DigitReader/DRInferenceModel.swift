//
//  DRInferenceModel.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/5/20.
//

import Foundation
import CoreGraphics


class DRInferenceModel: ObservableObject {
    @Published var inferenceResult: DDCNNInference
    private var network: DRNetworkContainer
    
    init() {
        network = DRNetworkContainer()
        inferenceResult = DDCNNInference(
            image: network.createEmptyMNISTImage(),
            softmaxDistribution: Array(repeating: Float(0.0), count: 10),
            maxValue: 0
        )
    }
    
    func inferDigit(from drawnImage: MPImage) {
        print(">>> Pre-processing image for network...")
        if let convertedImage = network.processMNISTImageForNetwork(from: drawnImage.cgImage!) {
            inferenceResult.image = convertedImage
            print(">>> Image processed.")
        } else {
            inferenceResult.image = network.createEmptyMNISTImage()
        }
        if let distribution = network.performSoftMaxInference(on: inferenceResult.image) {
            inferenceResult.softmaxDistribution = distribution
            inferenceResult.maxValue = distribution.firstIndex(of: distribution.max() ?? 0.0) ?? 0
        } else {
            inferenceResult.softmaxDistribution = Array(repeating: Float(0.0), count: 10)
        }
    }
    
}

struct DDCNNInference {
    var image: CGImage
    var softmaxDistribution: [Float]
    var maxValue: Int
}






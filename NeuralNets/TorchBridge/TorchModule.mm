//
//  TorchModule.mm
//  Numeral
//
//  Created by Mark Matson on 8/12/20.
//  Copyright © 2020 Mark Matson. All rights reserved.
//

#import "TorchModule.h"
#import <LibTorch/LibTorch.h>

@implementation TorchModule {
 @protected
  torch::jit::script::Module net;
}

- (nullable instancetype)initWithFileAtPath:(NSString*)filePath {
  self = [super init];
  if (self) {
    try {
      net = torch::jit::load(filePath.UTF8String);
      net.eval();
    } catch (const std::exception& exception) {
      NSLog(@"%s", exception.what());
      return nil;
    }
  }
  return self;
}

- (NSArray*)predictImage:(void*)imageBuffer {
    try {
        at::Tensor tensor = torch::from_blob(imageBuffer, {1, 1, 28, 28}, at::kFloat);
        torch::autograd::AutoGradMode guard(false);
        at::AutoNonVariableTypeMode non_var_type_mode(true);
        auto outputTensor = net.forward({tensor}).toTensor();
        float* floatBuffer = outputTensor.data_ptr<float>();
            if (!floatBuffer) {
                return nil;
            }
            NSMutableArray* results = [[NSMutableArray alloc] init];
            for (int i = 0; i < 10; i++) {
                [results addObject:@(floatBuffer[i])];
            }
            return [results copy];
    } catch (const std::exception& exception) {
        NSLog(@"%s", exception.what());
    }
    return nil;
}

@end

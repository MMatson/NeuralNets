//
//  TorchModule.h
//  NeuralNets
//
//  Created by Mark Matson on 11/20/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TorchModule : NSObject

- (nullable instancetype)initWithFileAtPath:(NSString*)filePath
    NS_SWIFT_NAME(init(fileAtPath:))NS_DESIGNATED_INITIALIZER;
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
- (nullable NSArray*)predictImage:(void*)imageBuffer NS_SWIFT_NAME(inferSoftmaxDistribution(image:));

@end

NS_ASSUME_NONNULL_END

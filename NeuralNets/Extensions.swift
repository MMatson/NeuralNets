//
//  Extensions.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/5/20.
//

import Foundation
import CoreGraphics
import CoreImage

//MPImage (Multi-Platform)
#if os(iOS)
    import UIKit
    public typealias MPImage = UIImage
#elseif os(macOS)
    import AppKit
    public typealias MPImage = NSImage
#endif

//extension CIImage {
//    func toCGImage(context: CIContext? = nil) -> CGImage? {
//        let ctx = context ?? CIContext(options: nil)
//        return ctx.createCGImage(self, from: self.extent)
//    }
//}


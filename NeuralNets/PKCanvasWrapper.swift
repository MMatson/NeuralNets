//
//  PKCanvasWrapper.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/1/20.
//

import SwiftUI
import PencilKit

struct PKCanvasWrapper: UIViewRepresentable {
    @Binding var canvasView: PKCanvasView
    
    func makeUIView(context: Context) -> PKCanvasView {
        self.canvasView.backgroundColor = .white
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first, let toolPicker = PKToolPicker.shared(for: window) else {
            return canvasView
        }
        toolPicker.setVisible(true, forFirstResponder: canvasView)
        toolPicker.addObserver(canvasView)
        canvasView.becomeFirstResponder()
        return canvasView
    }
    
    func updateUIView(_ uiView: PKCanvasView, context: Context) { }
}

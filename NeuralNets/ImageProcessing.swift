//
//  ImageProcessing.swift
//  NeuralNets
//
//  Created by Mark Matson on 12/4/20.
//

import Foundation
import CoreGraphics



private func resize(image: CGImage, newSize: CGSize, padding: CGFloat = 0) -> CGImage? {
    
    let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    let imageSize = CGSize(width: image.width, height: image.height)
    let widthRatio = (newSize.width - (padding * 2)) / imageSize.width
    let heightRatio = (newSize.height - (padding * 2)) / imageSize.height

    print("width: \(image.width), height: \(image.height), widthRatio: \(widthRatio), heightRatio: \(heightRatio)")
    
    var scale: CGFloat
    widthRatio < heightRatio ? (scale = widthRatio) : (scale = heightRatio)
    
    let imageRect = CGRect(
        x: (newSize.width - (imageSize.width * scale)) / 2,
        y: (newSize.height - (imageSize.height * scale)) / 2,
        width: imageSize.width * scale,
        height: imageSize.height * scale
    )
    print("scale: \(scale)")
    guard let context = CGContext(
            data: nil,
            width: Int(newSize.width),
            height: Int(newSize.height),
            bitsPerComponent: image.bitsPerComponent,
            bytesPerRow: image.bytesPerRow,
            space: image.colorSpace!,
            bitmapInfo: image.bitmapInfo.rawValue
    )
    else {
        return nil
    }
    print("mnistRect: \(newRect)")
    print("imageRect: \(imageRect)")
    context.setFillColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
    context.fill(newRect)
    //context.draw(createEmptyMNISTImage(), in: mnistRect)
    //context.interpolationQuality = .high
    context.draw(image, in: imageRect)
    return context.makeImage()!
}

//
//  SoftMaxOutputCard.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/3/20.
//

import SwiftUI
import Charts

struct SoftMaxOutputCard: View {
    @ObservedObject var model: DRInferenceModel
    
    var body: some View {
        Chart(data: model.inferenceResult.softmaxDistribution.reversed())
            .chartStyle(
                ColumnChartStyle(column: Capsule().foregroundColor(Color("Ink")), spacing: 5)
            )
    }
}


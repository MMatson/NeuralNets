//
//  ContentView.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/1/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            VStack {
                Divider()
                
                // Embed in list once a second network is created
                NavigationLink(
                    destination: DRView(),
                    label: {
                        HStack {
                            Text("Digit Reader")
                                .font(.title)
                                .fontWeight(.medium)
                                .foregroundColor(Color("Ink"))
                                .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                            Spacer()
                            Image(systemName: "4.alt.square")
                                .font(/*@START_MENU_TOKEN@*/.largeTitle/*@END_MENU_TOKEN@*/)
                                .foregroundColor(Color("Ink"))
                                .padding(.all)
                        }
                    })
                
                Divider()
                Spacer()
            }.navigationBarTitle("Neural Net Tests")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

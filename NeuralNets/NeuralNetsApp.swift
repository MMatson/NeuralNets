//
//  NeuralNetsApp.swift
//  NeuralNets
//
//  Created by Mark Matson on 10/1/20.
//

import SwiftUI

@main
struct NeuralNetsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
